#pragma once

#include <qapplication.h>
#include <qglviewer.h>

#include <srrg_nicp_local_mapper/local_mapper.h>

#include <srrg_core_map_viewers/trajectory_viewer.h>

namespace srrg_nicp_local_mapper_gui {
  
  class LocalMapperViewer : public srrg_core_map_gui::TrajectoryViewer {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    LocalMapperViewer(srrg_nicp_local_mapper::LocalMapper* _local_mapper);

    virtual void draw();

  protected:
    srrg_nicp_tracker::Tracker* _tracker;
    srrg_nicp_local_mapper::LocalMapper* _local_mapper;
    bool _modelTainted;
    srrg_core_map::MapNodeList* _local_map_trajectory;

  };
  
}
